package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class CounterMapper implements Converter<Long, MovieCounterDto> {


    @Override
    public MovieCounterDto convert(Long from) {
        return new MovieCounterDto(from);
    }
}
