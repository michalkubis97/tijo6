package pl.edu.pwsztar.service.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.mapper.CounterMapper;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import pl.edu.pwsztar.service.MovieService;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieServiceImpl.class);

    private final MovieRepository movieRepository;
    private final Converter <CreateMovieDto, Movie> convertToEntity;
    private final Converter <List<Movie>, List<MovieDto>> convertToDto;
    private final Converter <Long, MovieCounterDto> counterMapper;


    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository, Converter<Long, MovieCounterDto> counterMapper,
                            Converter<CreateMovieDto, Movie> convertToEntity, Converter<List<Movie>, List<MovieDto>> convertToDto) {

        this.movieRepository = movieRepository;
        this.convertToEntity = convertToEntity;
        this.convertToDto = convertToDto;
        this.counterMapper = counterMapper;
    }

    @Override
    public List<MovieDto> findAll() {
        List<Movie> movies = movieRepository.findAll();
        return convertToDto.convert(movies);
    }

    @Override
    public void creatMovie(CreateMovieDto createMovieDto) {
        Movie movie = convertToEntity.convert(createMovieDto);
        movieRepository.save(movie);
    }

    @Override
    public void deleteMovie(Long movieId) {
        Optional<Movie> movieOptional = movieRepository.findById(movieId);
        movieOptional.ifPresent(movieRepository::delete);
    }
    public MovieCounterDto getCounter() {
       return counterMapper.convert(movieRepository.movieCount());
    }
}
